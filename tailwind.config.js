module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  theme: {
    extend: {
      colors: {
        "primary-green": "#00B371",
        "primary-blue": "#1D3153",
        "primary-blue-dark": "#0e1929",
      },
    },
  },
  plugins: [require("@tailwindcss/aspect-ratio")],
};
